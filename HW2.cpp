#include <iostream>
#include <chrono>
using namespace std;

int main() {
	int n, i, j;

	cout << "Number of rows and columns in matrix: ";
	cin >> n;

	int* A = (int*) malloc(n * n * sizeof(int));
	int* B = (int*) malloc(n * n * sizeof(int));
	for (i = 0; i < n; i++){
		for (j = 0; j < n; j++){
			*(A + i * n + j) = i;
		}
	}

	chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	for (i = 0; i < n; i++){
		for (j = 0; j < n; j++){
			*(B + i * n + j) = *(A + j * n + i);
		}
	}

	chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
	cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << " microseconds" <<std::endl;

	delete [] A;
	delete [] B;
	return 0;
}
